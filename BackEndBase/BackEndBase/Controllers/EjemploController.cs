﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BackEndBase.Models;
using BackEndBase.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace BackEndBase.Controllers
{
    [Route("api/Ejemplo")]
    [ApiController]
    public class EjemploController : ControllerBase
    {
        private readonly IEjemploService _ejemploService;
        public EjemploController(IEjemploService ejemploService)
        {
            _ejemploService = ejemploService;
        }

        // GET: api/Ejemplo
        [HttpGet]
        public IActionResult GetItemList()
        {
            return Ok(_ejemploService.GetItemList());

        }

        // POST: api/Ejemplo
        [HttpPost]
        public IActionResult AddItem([FromBody] ItemModel item)
        {
            _ejemploService.AddItem(item);
            return Ok();
        }


        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public IActionResult Delete([FromBody] ItemModel item)
        {
            _ejemploService.RemoveItem();
            return Ok();
        }
    }
}
