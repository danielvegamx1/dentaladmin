﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BackEndBase.Models;

namespace BackEndBase.Services
{
    public class EjemploService : IEjemploService
    {
        
        private readonly Dictionary<string, int> _ListItems = new Dictionary<string, int>();

        public void AddItem(ItemModel item)
        {
            _ListItems.Add(item.Nombre,item.Cantidad);
        }

        public Dictionary<string,int> GetItemList()
        {
      
            return _ListItems;
        }

        public void RemoveItem()
        {
            throw new NotImplementedException();
        }
    }

}
