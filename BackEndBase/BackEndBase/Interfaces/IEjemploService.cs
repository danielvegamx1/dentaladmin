﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BackEndBase.Models;

namespace BackEndBase.Services
{
    public interface IEjemploService
    {
      Dictionary<string, int> GetItemList();
        void AddItem(ItemModel item);
        void RemoveItem();
    }
}
