﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BackEndBase.Models
{
    public class ItemModel
    {
        public string Nombre { get; set; }
        public int Cantidad { get; set; }
    }
}
